# CountDown


## Name
CountDown - 

## Description
The app replicates the rounds within the popular UK television game-show, CountDown. If you are not familiar with how it works, a good starting point is available on [Wikipedia - Countdown Format](https://en.wikipedia.org/wiki/Countdown_(game_show)#:~:text=its%2071st%20series.-,Format,-%5Bedit%5D).

The concepts of the game itself is not complex, however, the project as been undertaken as a way of coding a project entirely in Kotlin (without my much love Java). 

Concepts used will include, but is not limited to Activity, Fragment, UI binding, MVP where applicable, InputStream, e.t.c.

## Project status
Work-in-Progress
